Welcome to A Planet's Revenge, called APR for short.
What type of game is APR? It is a text-based, turn-based 4X game.
Being text-based means that everything is represented using text, and you use
a keyboard to play. Turn-based means that you have as long to think about
your actions as you want, without any time pressure. And finally, 4X is the
genre of APR and stands for eXplore, eXpand, eXploit, and eXterminate.
Without going into details, lets just say in APR you will be exploring for
new planets, colonising and developing them, building fleets of ships,
and crushing enemies, amoung other pursuits.

To understand APR, you will need to know the following about game controls:
- Everything in APR uses single key presses. There is no time where you
  will need to enter a series of characters and press enter.
- From any screen, press BACKSPACE to return to the previous screen.
- From the first screen, press ENTER to end your turn.
- From the first screen, press ESCAPE to leave the game.
- Whenever you see a capitalized word, like SPACE or a number/letter in
  brackets, like (1) or (a), that means you can press that button to perform
  an action, which is normally written next to the key to press.

Also, you will need to know the following about the turn-based nature of APR:
- Every action uses ACTION POINTS, which are called AP for short.
- The only way to gain action points is to end your turn.
- Ending your turn also gains you resources from your planets.

And finally, you will need to know about how things on planets work:
- Each planet has six different types of infrastructure, which must be raised
  if you are to succeed in APR.
- Life Support raised your population cap, and how fast the population grows.
- Mining and Agriculture raises the speed at which you get certain resources.
- Commerce raises the amount of credits you get per turn from population.
- Industry makes you build ships faster, Research affects your research speed.
- Each planet also has a population, who needs food to survive and grow..

With all of the above things in mind, try and explore/colonise a planet and
build some infrastructure on it. From there, you'll need to try out the other
things available in APR to succeed.

If you get stuck, refer back to this tutorial.
View the README file for detailed game information and help.
Look at the "complete list of controls" for all the areas you may go to.

Well, that's it...good luck!
